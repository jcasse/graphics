# A Minimal Ray Tracer

From Graphics Gems IV by Paul S. Heckbert, page 375.

Creates reflective spheres and shoots rays of light, tracing them to
create an image.

Note: Something is not working because the spheres are not showing.

## Requirements

- C++

## Compiling

```Bash
g++ raytracer.cpp
```

## Running

```Bash
./a.out > picture.jpg
```
